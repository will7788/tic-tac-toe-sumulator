﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_Project_4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void NewGame_Click(object sender, EventArgs e)
        {
            tacToe();


        }
        private void tacToe()
        {
            //re-setting all of the text boxes
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";
            textBox9.Text = "";
            //create random object and all of the arrays needed
            Random r2 = new Random();
            int[,] board = new int[3, 3]{
                { -1, -1, -1 },
                { -1, -1, -1 },
                { -1, -1, -1 }
            };
            string[,] strBoard = new string[3, 3];
            object[,] textBoxes = new object[3, 3] {
                { textBox1, textBox2, textBox3 },
                { textBox4, textBox5, textBox6 },
                { textBox7, textBox8, textBox9 }
            };
            TextBox new1 = (TextBox)textBoxes[1, 1];
            Queue<int> oneZeroInt = new Queue<int>(new[] { 1, 0, 1, 0, 1, 0, 1, 0, 1 });
            Queue<string> oneZeroStr = new Queue<string>(new[] { "X", "O", "X", "O", "X", "O", "X", "O", "X" });
            bool full = false;
            bool xWins = false;
            bool oWins = false;
            int rand1;
            int rand2;
            int count = 0;
            //loop through and take each turn one at a time, rather than randomly populating the entire board
            while (!full && (!oWins && !xWins))
            {
                rand1 = r2.Next(3);
                rand2 = r2.Next(3);
                if (board[rand1, rand2] == -1)
                {
                    board[rand1, rand2] = oneZeroInt.Dequeue();
                    strBoard[rand1, rand2] = oneZeroStr.Dequeue();
                    var text = (TextBox)textBoxes[rand1, rand2];
                    text.Text = strBoard[rand1, rand2];
                    count++;
                }
                //Evaluate O wins horizontal
                if (board[0, 0] == 0 && board[0, 1] == 0 && board[0, 2] == 0) { oWins = true; }
                if (board[1, 0] == 0 && board[1, 1] == 0 && board[1, 2] == 0) { oWins = true; }
                if (board[2, 0] == 0 && board[2, 1] == 0 && board[2, 2] == 0) { oWins = true; }
                //Evaluate O wins vertical
                if (board[0, 0] == 0 && board[1, 0] == 0 && board[2, 0] == 0) { oWins = true; }
                if (board[0, 1] == 0 && board[1, 1] == 0 && board[2, 1] == 0) { oWins = true; }
                if (board[0, 2] == 0 && board[1, 2] == 0 && board[2, 2] == 0) { oWins = true; }
                //Evaluate O wins diagonal
                if (board[0, 0] == 0 && board[1, 1] == 0 && board[2, 2] == 0) { oWins = true; }
                if (board[0, 2] == 0 && board[1, 1] == 0 && board[2, 0] == 0) { oWins = true; }
                //Evaluate X wins horizontal
                if (board[0, 0] == 1 && board[0, 1] == 1 && board[0, 2] == 1) { xWins = true; }
                if (board[1, 0] == 1 && board[1, 1] == 1 && board[1, 2] == 1) { xWins = true; }
                if (board[2, 0] == 1 && board[2, 1] == 1 && board[2, 2] == 1) { xWins = true; }
                //Evaluate X wins vertical
                if (board[0, 0] == 1 && board[1, 0] == 1 && board[2, 0] == 1) { xWins = true; }
                if (board[0, 1] == 1 && board[1, 1] == 1 && board[2, 1] == 1) { xWins = true; }
                if (board[0, 2] == 1 && board[1, 2] == 1 && board[2, 2] == 1) { xWins = true; }
                //Evaluate X wins diagonal
                if (board[0, 0] == 1 && board[1, 1] == 1 && board[2, 2] == 1) { xWins = true; }
                if (board[0, 2] == 1 && board[1, 1] == 1 && board[2, 0] == 1) { xWins = true; }

                //Win cases
                if (xWins && !oWins) { resultBox.Text = "X Wins!"; }
                if (!xWins && oWins) { resultBox.Text = "O Wins!"; }
                //both win should not happen as I am checking every turn but you never know
                if (xWins && oWins) { resultBox.Text = "Cats Game!"; }
                if (!xWins && !oWins) { resultBox.Text = "Cats Game!"; }

                if (count == 9) { full = true; }
            }
        }
    }
}
